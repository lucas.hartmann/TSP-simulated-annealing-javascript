// (c) 2018 Lucas Hartmann.
// License GNU Affero-GPL.

var distMode = 0;

self.onmessage = function(m) {
  var cities = m.data.cities;
  var tolerance = m.data.tolerance;
  var quiet = m.data.quiet;
  if (m.data.distMode)
    distMode = m.data.distMode;

  // %norm = (1-%mut)**n
  tolerance = 1-(1-tolerance)**(1/cities.length**2);
  console.log(m.data);

  var tmStarted = new Date();
  var elapsed = 10;

  var curDist  = calcDistance(cities);

  var stage = 0;
  var final = false;
  var updateui = function(extra) {
    if (!quiet || final) {
      var tmNow = new Date();
      if (tmNow - tmStarted > elapsed || final) {
        elapsed += 100;
        var msg = {
          stage:  stage,
          cities: cities,
          time:   tmNow - tmStarted
        };
        for (var prop in extra) {
          msg[prop] = extra[prop];
        }
        postMessage(msg);
      }
    }
  }
  var anneal = function () {
    var changed = false;
    for (var left = 0; left < cities.length; left++) {
      for (var right = left+1; right < cities.length; right++) {
        var head = cities.slice(0, left);
        var body = cities.slice(left, right);
        var tail = cities.slice(right);

        // Test 1: reverse substring
        var test = head.concat(body.reverse()).concat(tail);
        // var test = joinStrings(head.concat(tail), body);
        var curDist  = calcDistance(cities);
        var distance = calcDistance(test);

        if (distance < curDist || distance/curDist * Math.random() < tolerance) {
          cities = test.slice();
          curDist = distance;
          changed = true;
        }

        updateui({left:left, right:right});
      }
    }
    return changed;
  }
  var reinsert = function(low, high) {
    for (var i=high; i >= low; i--) {
      for (var j=0; j < cities.length-i; j++) {
        var city = cities.splice(j,i);
        cities = joinStrings(city, cities);

        updateui({
          substring_length: i,
          left: j,
          right: j+i
        });
      }
    }
  }

  // Process execution
  var step = 0;
  stage = 0;
  while (anneal());
  if (true) { // Additional passes
    stage = 1;
    reinsert(1,4);
    stage = 2;
    while (anneal());
  }
  stage = 3;
  final = true;
  updateui();
  console.log('Worker finished.');
}

function calcDistance(cities) {
  var sum = 0;
  for (var i=0; i<cities.length; i++)
    sum += cityDistance(cities[i], cities[(i+1)%cities.length]);
  return sum;
}

function cityDistance(a,b) {
  var dx = Math.abs(a.x-b.x);
  var dy = Math.abs(a.y-b.y);

  switch (distMode) {
    // Euclidian distance, straight line
    case 0: return Math.sqrt(dx*dx + dy*dy);

    // Manhattan distance, only horizontal and vertical moves allowed.
    case 1: return dx + dy;

    // 8-way Manhattan: Include 45-degree diagonals as viable moves.
    case 2: return Math.min(dx,dy) * 1.4142 + Math.abs(dx-dy);

    // CNC fast-move approximation: Machines can usually move at full speed on
    // X and Y simultaneously, so only the worst case matters.
    case 3: return Math.max(dx, dy);

    // I still find diagonals ugly and they add unnecessary axis wear to CNC,
    // so add 25% axis movement to the equation so we try to minimize it.
    case 4: return Math.max(dx, dy) + 0.25 * (dx + dy);
  }

  // Fail
  return 0;
}

var joinStrings = function(A, B) {
  var Ar = A.slice().reverse();
  var bestPath = A.concat(B);
  var bestDist = calcDistance(bestPath);
  for (var i = 0; i < B.length; i++) {
    // Try injecting normally
    var testPath = B.slice(0,i).concat(A).concat(B.slice(i));
    var testDist = calcDistance(testPath);

    if (testDist < bestDist) {
      bestPath = testPath;
      bestDist = testDist;
    }

    // Try injecting backwards
    if (A.length < 2) continue;
    var testPath = B.slice(0,i).concat(Ar).concat(B.slice(i));
    var testDist = calcDistance(testPath);

    if (testDist < bestDist) {
      bestPath = testPath;
      bestDist = testDist;
    }
  }
  return bestPath;
}
