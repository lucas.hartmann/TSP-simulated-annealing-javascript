# TSP-simulated-annealing-javascript
(c) 2018 Lucas Hartmann.

Travelling salesperson solver in javascript, inspired on work from CodingTrain's Coding Challenge 35.

Path optimization algorithm is custom:
- First pass is simulated annealing.
- Second pass is reinsert, where a subpath is removed from the main loop, and added to the where it causes the least increase in cost.
- Third pass is suimulated annealing again, just for cleanup.

Five distinct cost functions are implemented:
- Euclidian distance, or straight line: Creates the most comprehensible routes, meaningful if movements are constant speed, like cars on highways. Cost = sqrt(dx*dx + dy*dy).
- Manhattan distance: Total cost is the sum of vertical and horizontal displacements. Should be used when moves can only be performed on a single axis at a time, like on a city with square bocks.
- 8-way Manhatan: Similar to Manhatan, but 45 degree diagonals are also allowed. Cost = max(dx,dy) + (sqrt(2)-1) * min(dx,dy).
- CNC fast move (G00) duration: Cartesian CNC machines, such as those for drilling and pick-and-placing, have independent X and Y motors. This means that the speed on each axis can be controlled independently, and the machine can move faster diagonally than horizontally or vertically. The total time of each move depends only on the axis with the longes displacement, so cost=max(dx,dy). Employing this const function usually leads to routes that an ill-advised user would consider wrong, they are not, they are just weird.
- Modified CNC: Similar to above, but adding axis movement as part of the cost function. This leads to paths with less crossings, reduces axis movement and wear, and makes paths less uncomfortable for users to look at. Cost = max(dx,dy) + 0.25*(dx+dy).

All path optimization code is on anneal_worker.js. All visualization code is on sketch.js.

License: GNU AGPL, latest version.
https://www.gnu.org/licenses/agpl-3.0.html
