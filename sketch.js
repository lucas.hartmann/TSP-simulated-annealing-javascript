// (c) 2018 Lucas Hartmann.
// Loosely based on work from Daniel Shiffman, http://codingtra.in

var cities = [];
var totalCities = 500;
var distMode = 0;
var citySize = 0;
var done = false;
var changed = true;
var elapsed = 0;
var stage = 0;
var colorset = [
  [255,255,255],
  [255,128+64,128],
  [288,255,0],
  [0,255,0]
];
var distModeLabel = [
  "Euclidian = sqrt(dx*dx + dy*dy)",
  "Manhatan = abs(dx) + abs(dy)",
  "8-way Manhatan = min(abs(dx), abs(dy))*sqrt(2) + abs(abs(dx)-abs(dy))",
  "CNC fast move duration = max(abs(dx), abs(dy))",
  "Modified CNC fast move = max(dx,dy) + 0.25 * (dx+dy)",
];
var stageLabel = [
  "Simulated annealing (pass 1)...",
  "Reinsert (substring.length = XXX)...",
  "Simulated annealing (pass 2)...",
  "Done."
];
var substring_length = 0;
var extraData = {};

var canvasWidth = 0;
var canvasHeight = 0;
var pcbScale = 0;
var pcbLeft=0, pcbRight=0, pcbTop=0, pcbBottom=0;

var worker = new Worker("anneal_worker.js");
worker.onmessage = function(msg) {
  // console.log(msg);
  cities = msg.data.cities;
  elapsed = msg.data.time;
  stage = msg.data.stage;
  extraData = msg.data;

  done = stage >= 3;
  changed = true;
};

function preload() {
  if (random() < 0.5) {
    // Cities represent printed circuit board drills, for machining.
    cities = loadJSON("cities.json");
  } else {
    // Cities are random points.
    var width = document.body.scrollWidth;
    var height = document.body.scrollHeight;
    for (var i = 0; i < totalCities; i++) {
      var v = {x:random(width), y:random(height)};
      cities[i] = v;
    }
  }

  // Choose a random distance mode 0..length-1
  distMode = Math.floor(Math.random() * distModeLabel.length);
}

function windowResized(w,h) {
  canvasWidth = windowWidth;
  canvasHeight = windowHeight;
  resizeCanvas(canvasWidth, canvasHeight);
  changed = true;
}

function setup() {
  canvasWidth = document.body.scrollWidth;
  canvasHeight = document.body.scrollHeight;
  createCanvas(canvasWidth, canvasHeight);

  // Make sure cities is an array
  cities = Object.values(cities);

  // Figure out cities bounding rectangle
  pcbTop = +Infinity;
  pcbLeft = +Infinity;
  pcbBottom = -Infinity;
  pcbRight = -Infinity;
  cities.forEach((c) => {
    if (pcbTop    > c.y) pcbTop    = c.y;
    if (pcbLeft   > c.x) pcbLeft   = c.x;
    if (pcbBottom < c.y) pcbBottom = c.y;
    if (pcbRight  < c.x) pcbRight  = c.x;
  });
  console.log("Cities extends X[" + pcbLeft + ", " + pcbRight + "], Y[" + pcbBottom + "," + pcbTop + "].");

  // Figure how big to draw cities
  citySize = +Infinity;
  cities.forEach((a) => { cities.forEach((b) => {
    var d = dist(a.x,a.y, b.x,b.y);
    if (a!==b) citySize = min(citySize, d);
  })});
  citySize = max(citySize / 2, dist(pcbLeft, pcbTop, pcbBottom, pcbRight)/300);

  // Start the worker
  worker.postMessage({
    cities: cities,
    tolerance: 0.01,
    quiet: false,
    distMode: distMode
  });
}

var delayLock = 10;
function draw() {
  // Skip redraw if not changed
  if (!changed) return;

  if (done) noLoop();

  // Set position and scaling
  fixScaling();
  translate(canvasWidth/2, canvasHeight/2);
  scale(pcbScale);
  translate(-(pcbLeft+pcbRight)/2, -(pcbBottom+pcbTop)/2);

  // Draw:
  background(0);

  // Draw the path
  stroke(colorset[stage]);
  strokeWeight(1/pcbScale);
  fill([255,255,255,30]);
  beginShape();
  cities.forEach(c => vertex(c.x, c.y));
  vertex(cities[0].x, cities[0].y);
  endShape();

  // Draw the selected parts
  if (extraData.left && extraData.right) {
    if (stage == 0 || stage == 2) {
      strokeWeight(3/pcbScale);
      stroke([0,0,255]);
      noFill();

      var a = extraData.left;
      var b = (a+1) % cities.length;
      beginShape();
      vertex(cities[a].x, cities[a].y);
      vertex(cities[b].x, cities[b].y);
      endShape();

      a = extraData.right;
      b = (a+1) % cities.length;
      beginShape();
      vertex(cities[a].x, cities[a].y);
      vertex(cities[b].x, cities[b].y);
      endShape();
    }
  }

  // Draw the cities
  stroke(colorset[stage]);
  strokeWeight(1/pcbScale);
  fill(done ? [0,255,0] : [255,255,255]);
  for (var i = 0; i < cities.length; i++) {
    ellipse(cities[i].x, cities[i].y, citySize, citySize);
  }
  ellipse(cities[0].x, cities[0].y, 2*citySize, 2*citySize);
  ellipse(cities[cities.length-1].x, cities[cities.length-1].y, 2*citySize, citySize);

  //
  if (extraData.left && extraData.right && stage == 1) {
    stroke([0,0,255]);
    fill([0,0,255]);
    for (var i = extraData.left; i < extraData.right; i++) {
      ellipse(cities[i].x, cities[i].y, 2*citySize, 2*citySize);
    }
  }

  // Draw the timer
  resetMatrix();
  textAlign(CENTER, CENTER);
  textSize(128);
  strokeWeight(1);
  stroke(done ? [0,255,0,255] : [255,0,0,255]);
  fill(done ? [0,255,0,255/3] : [255,0,0,255/3]);
  text(nf(elapsed/1000, 1, 1), canvasWidth/2, canvasHeight/2);

  // Draw the extra information
  resetMatrix();
  textAlign(LEFT,BOTTOM);
  textSize(16);
  fill(255);
  noStroke();
  var txt = stageLabel[stage] + "\n" + distModeLabel[distMode] + ".\n(c) 2018 LHartmann.";
  if (extraData.substring_length) {
    txt = txt.replace("XXX", extraData.substring_length);
  }
  text(txt, 10, canvasHeight-10);
}

function fixScaling() {
  pcbScale = 0.9 * min(canvasWidth / (pcbRight-pcbLeft), canvasHeight / (pcbBottom - pcbTop));
}
